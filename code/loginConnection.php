<?php
// Import the Connector class
require_once ('connector.php');

// Grab username and password from the submission form
$username = htmlspecialchars($_POST['email']);
$password = htmlspecialchars($_POST['password']);

// Create a connector using the submitted information
$con = new Connector($username, $password, '73011639c6ae6350de5e4725d720dd8a');

// Connect
$result = $con->connect();

$twohours = time()+(3600*2);
$oneweek = time()+(3600*168);
setcookie("user", $username, $oneweek);
// 'EASW_KEY' => $EASW_KEY, 'EASF_SESS' => $EASF_SESS, 'XSID' => $XSID, 'PHISHKEY' => $PHISHKEY
setcookie("EASW_KEY", $result["EASW_KEY"], $twohours);
setcookie("EASF_SESS", $result["EASF_SESS"], $twohours);
setcookie("XSID", $result["XSID"], $twohours);
setcookie("PHISHKEY", $result["PHISHKEY"], $twohours);

// echo of the returned value from connect()
echo json_encode($result);

//echo "true";
?>