// Logout on link click
$(function() {
    $("#logout_btn").click(function() {
        
        // Use Ajax to process the logout submission
        $.ajax({
            type: "POST",
            url: "logoutConnection.php",
            success: function(data) {
                $("#search_form").html("<div id=message></div>");
                $("#message").html("<h2>Logged out successfully.</h2>")
                    .hide()
                    .fadeIn(1500, function() {
                        window.location.replace(indexurl);
                    });
            }
        });
        return false;
    });
});