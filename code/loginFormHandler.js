// Focus the cursor on the login box on page load.
$(document).ready(function() {
    $("input#email").focus();
     var username = readCookie("user");
     if (username) {
        $("input#email").val(username);
        $("input#password").focus();
     }
});

// Form submission on button click
$(function() {
    $("input#submit_btn").click(function() {
        $("#login_form").fadeOut(500, function() {
            $("#updates").hide();
            $("#updates").fadeIn(3000);
            $("#updates").html("<h1>Processing...</h1>");
        });
        
        // Grab values from the form
        var username = $("input#email").val();
        if (username == "") {
            $("input#email").focus();
        }
        var password = $("input#password").val();
        if (password == "") {
            $("input#password").focus();
        }
        
        // Use Ajax to process the form submission
        var dataString = "email=" + username + "&password=" + password;
        var returnedData = null;
        $.ajax({
            type: "POST",
            url: "loginConnection.php",
            data: dataString,
            success: function(connectionstring) {
                var constring = connectionstring;
                
                /**
                 * Login validation commented out until we figure out this damn phishkey problem.
                // Get the xsid value
                var xsid_index = constring.indexOf("XSID") + "XSID\":\"".length;
                var xsid_key = constring.substring(xsid_index);
                xsid_key = xsid_key.substring(0, xsid_key.indexOf("\""));
                
                var valid = false;
                if (xsid_key.indexOf("X-UT-SID") != -1) {
                    valid = true;
                } else {
                    valid = false;
                }
                */
                
                // Test code:
                $("#updates").html('return value: ' + constring);
                return false; // To kill the page so that we can actually look at the debug
                // End test code
                
                if (valid) {
                    $("#updates").html('<h2>EA Login Processed! </h2>')
                        .hide()
                        // fade in, then navigate to the logged-in page
                        .fadeIn(1000, function() {
                            window.location.replace(sessionurl);
                        });
                } else {
                    $("#login_form").fadeIn();
                    $("input#password").css("color", "gray")
                        .focus()
                        .select();
                    $("#updates").hide();
                }
            }
        });
        return false;
    });
});