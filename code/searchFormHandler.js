// Focus the cursor on the search box and display an example input on page load.
$(document).ready(function() {
    // Check if they aren't logged in
    var xsid = readCookie("XSID");
    if (xsid === "Content-Length: 0" || xsid === '') {
        window.location.replace(sessionurl);
    }
    
    $("input#player").val("Fabregas");
    $("input#player").focus(function(){
        this.select();
    });
    
});



// Form submission on button click
$(function() {
    $("input#submit_btn").click(function() {
        $("#search_results").html("<h1>Processing...</h1>")
            .hide()
            .fadeIn(2000);
        
        
        // Grab values from the form
        var playerName = $("input#player").val();
        if (playerName == "") {
            $("input#player").focus();
            $("#search_results").fadeOut(2000);
            return false;
        }
        
        var dataString = "player="+playerName;
        // Use Ajax to process the form submission.
        $.ajax({
            type: "POST",
            url: "searchDatabase.php",
            data: dataString,
            success: function(retval) {
                //var result = $.parseJSON(retval);
                var result = retval;
                $("#search_results").html("<body> Search Processed! data: " + result + " </body>")
                            .hide()
                            .fadeIn(1000);
            }
        });
        return false;
    });
});